const express = require("express");
const rp = require("request-promise");
const app = express();
const http = require("http");
const server = http.createServer(app);
const port = process.env.PORT || 8088;

app.set("view engine", "ejs");

function sortStrings(a, b) {
  a = a.toLowerCase();
  b = b.toLowerCase();
  return a < b ? -1 : a > b ? 1 : 0;
}

function sortNumber(a, b) {
  return a - b;
}

app.get("/", function(req, res) {
  let tagline = "Learn more about the Star Wars universe";
  res.render("pages/index", {
    tagline: tagline
  });
});

app.get("/characters", (req, res) => {
  console.log(req.query.sort);
  rp({
    uri: "https://swapi.co/api/people/",
    json: true
  })
    .then(data => {
      let parsedQuery = req.query.sort;
      let results = data.results;
      if (parsedQuery === "name") {
        results.sort(function(a, b) {
          return sortStrings(a.name, b.name);
        });
      }
      if (parsedQuery === "mass") {
        results.sort(function(a, b) {
          return sortNumber(a.mass, b.mass);
        });
      }
      if (parsedQuery === "height") {
        results.sort(function(a, b) {
          return sortNumber(a.height, b.height);
        });
      }
      res.render("pages/characters", {
        data: results
      });
      sortByName = false;
    })
    .catch(err => {
      console.log(err);
    });
});

app.get("/character/:name", (req, res) => {
  rp({
    uri: `https://swapi.co/api/people/?search=${req.params.name}`,
    json: true
  })
    .then(data => {
      res.render("pages/character", {
        data: data.results
      });
    })
    .catch(err => {
      // Deal with the error
      console.log(err);
    });
});

function extractNames(param) {
  return Promise.all(
    param.map(residentUri => {
      return rp({
        uri: residentUri,
        json: true
      });
    })
  )
    .then(residents => {
      const residentNames = residents.map(resident => {
        return resident.name;
      });
      return residentNames;
    })
    .catch(err => {
      console.log(err);
    });
}

function getPlanets(planets) {
  const result = {};
  return Promise.all(
    planets.map(planet => {
      const planetProcessed = planet;
      return extractNames(planetProcessed.residents).then(residentNames => {
        result[planet.name] = residentNames;
      });
    })
  ).then(() => {
    return result;
  });
}

app.get("/planetresidents", (req, res, next) => {
  rp({
    uri: "http://swapi.co/api/planets/",
    json: true
  })
    .then(data => {
      let planets = data.results;
      getPlanets(planets).then(p => {
        res.render("pages/planetresidents", {
          data: JSON.stringify(p)
        });
      });
    })
    .catch(err => {
      console.log(err);
    });
});

server.listen(port, "0.0.0.0", function() {
  server.close(function() {
    server.listen(port, "0.0.0.0");
  });
});
console.log("8088 is the magic port");
